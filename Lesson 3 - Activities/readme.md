# Занятие 3 - Activities 
 нужно знать перед занятием:
- [Life cycle](https://developer.android.com/guide/components/activities/activity-lifecycle#java)
- [Context](https://medium.freecodecamp.org/mastering-android-context-7055c8478a22)
- [Application class](https://github.com/codepath/android_guides/wiki/Understanding-the-Android-Application-Class)	
- startActivityForResult() 

### домашка:
в первом активити текстовое поле, пустое.  и кнопка “получить текст” по которой переходит на второе активити
во втором - инпут для текста и  кнопка назад
перенести текст из  второго активити в первое
ставим логи на жизненный цикл  
Портретная ориентация - элементы вертикально  
Альбомная ориентация - элементы горизонтально

